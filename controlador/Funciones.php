<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');
include_once($_SERVER['DOCUMENT_ROOT'] . '/pjimenez81dwspro6/controlador/Config.php');
include_once($_SERVER['DOCUMENT_ROOT'] . '/pjimenez81dwspro6/modelo/ModeloMysql.php');
include_once($_SERVER['DOCUMENT_ROOT'] . '/pjimenez81dwspro6/modelo/ModeloFicheros.php');

function recoge($valor) {
    if (isset($_REQUEST[$valor])) {
	$campo = htmlspecialchars(trim(strip_tags($_REQUEST[$valor])));
    } else {
	$campo = "";
    };
    return $campo;
}

function comprobarModelo() {

    $modeloElegido = "";
    if (Config::$modelo === "mysql") {
	$modeloElegido = new ModeloMysql();
    } else if (Config::$modelo === "ficheros") {
	$modeloElegido = new ModeloFicheros();
    }
    return $modeloElegido;
}

function cabecera() {
    echo "<h1>" . Config::$titulo . "</h1><hr/>\n";
}

function pie() {
    echo "<hr/><pre>" . Config::$empresa . " " . Config::$autor . " ";
    echo Config::$curso . " " . Config::$fecha . "</pre>\n";
}

function inicio() {
    echo "<align='right'><a href = '../index.php'>Inicio</a> </align>\n";
}

?>