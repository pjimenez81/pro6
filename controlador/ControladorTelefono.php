<?php
include_once($_SERVER['DOCUMENT_ROOT'] . '/pjimenez81dwspro6/modelo/ModeloFicheros.php');
include_once($_SERVER['DOCUMENT_ROOT'] . '/pjimenez81dwspro6/modelo/Telefono.php');


class ControladorTelefono {

    public function rellenarTablaTelefono() {
	
	$modeloFicheros = comprobarModelo();
	foreach ($modeloFicheros->leerTelefono() as $r):
	    ?>
	    <tr>
	        <td><?php echo $r->__GET('id'); ?></td>
	        <td><?php echo $r->__GET('numero'); ?></td>
	        <td><?php echo $r->__GET('idpersona')->__GET('id'); ?></td>
	    </tr>
	    <?php
	endforeach;
    }

    public function calcularIDTelefonos() {
	$modelo = comprobarModelo();
	$perros = $modelo->idTelefono();
	echo $perros;
    }

}
