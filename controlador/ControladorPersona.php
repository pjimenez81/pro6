<?php
include_once($_SERVER['DOCUMENT_ROOT'] . '/pjimenez81dwspro6/modelo/ModeloFicheros.php');
include_once($_SERVER['DOCUMENT_ROOT'] . '/pjimenez81dwspro6/modelo/Persona.php');


class ControladorPersona {

   public function rellenarCBPersonas() {
	$modelo = comprobarModelo();
	foreach ($modelo->leerPersona() as $r):
	    ?>
	    <option value ="<?php echo $r->__GET('id'); ?>"><?php echo $r->__GET('nombre'); ?></option>
	    <?php
	endforeach;
    }

    public function rellenarTablaPersona() {
	$modelo = comprobarModelo();
	foreach ($modelo->leerPersona() as $r):
	    ?>
	    <tr>
	        <td><?php echo $r->__GET('id'); ?></td>
	        <td><?php echo $r->__GET('nombre'); ?></td>
	    </tr>
	    <?php
	endforeach;
    }

    public function calcularIDPersonas() {
	$modelo = comprobarModelo();
	$personas = $modelo->idPersona();
	echo $personas;
    }

}
