<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Gestión de Personas</title>
        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.5.0/pure-min.css">
	<meta charset="UTF-8">
    </head>
    <body >
	<div>
	    <?php
	    error_reporting(E_ALL);
	    ini_set('display_errors', '1');
	    include_once($_SERVER['DOCUMENT_ROOT'] . '/pjimenez81dwspro6/controlador/Funciones.php');
	    include_once($_SERVER['DOCUMENT_ROOT'] . '/pjimenez81dwspro6/modelo/ModeloFicheros.php');
	    include_once($_SERVER['DOCUMENT_ROOT'] . '/pjimenez81dwspro6/modelo/ModeloMysql.php');

	    Config::$modelo = recoge('modelo');
	    echo "El modelo elegido para trabajar es " . Config::$modelo;
	    echo "<p>La base de datos comenzará a instalarse:</p>";
	    $modelo = comprobarModelo();
	    $modelo->instalarBD();
	    echo "<a href = ../vista/VistaPrincipal.php>Haga click aquí para continuar.</a><br/>";
	    ?>
	</div> 

    </body>