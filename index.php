<!DOCTYPE html>
<html>
    <head>
	<title>Bienvenido a gestión general</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
	<?php
	include_once($_SERVER['DOCUMENT_ROOT'] . '/pjimenez81dwspro6/controlador/Funciones.php');
        
	cabecera();
	?>
	<h1> Agenda Telefónica</h1>
	<form action="controlador/ControladorPrincipal.php" method="post">
	    <p>¿MySql o Fichero? Pulsa Escoger si deseas trabajar con Ficheros. Marca Mysql si quieres trabajar con Base de Datos.</p>
	    <div id="menu">
		<!--<input type="radio" name="modelo" value="mysql"> MySql<br />-->
		<button type="submit" name="modelo" value="ficheros">Fichero</button>
                <button type="submit" name="modelo" value="mysql">Mysql</button>
	    </div>
	</form>
	<div id="documentacion">
	    <p><a href="vista/VistaPrincipal.php">No Instalar Base de Datos.</a></p>
	    <p><a href="/pjimenez81dwspro6/documentacion/DWSProyectoDocumentacion.odt">Documentación en PDF.</a></p>
	</div>
	<?php
	pie();
	?>
    </body>
</html>
