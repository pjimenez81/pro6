<?php

include_once 'Persona.php';
include_once 'Telefono.php';
include_once 'Modelo.php';

class ModeloMysql implements Modelo {

    public function conectar() {
	$pdo = null;
	try {
	    $pdo = new PDO("mysql:host=localhost;dbname=" . Configuracion::$bdnombre, Configuracion::$bdusuario, Configuracion::$bdclave);
	    $pdo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, TRUE);
	} catch (PDOException $e) {
	    echo "<p>Error: No puede conectarse con la base de datos.</p>\n";
	    echo "<p>Error: " . $e->getMessage() . "</p>\n";
	}
	return $pdo;
    }

    public function desconectar() {
	return null;
    }

    public function instalarBD() {

	try {
	    $pdo = new PDO("mysql:host=localhost:8080", Config::$bdusuario, Config::$bdclave);
	    $pdo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, TRUE);
	} catch (PDOException $e) {
	    echo "<p>Error: No puede conectarse con la base de datos.</p>\n";
	    echo "<p>Error: " . $e->getMessage() . "</p>\n";
	}

	$consulta = "CREATE DATABASE IF NOT EXISTS " . Configuracion::$bdnombre;

	$pdo->query($consulta);

	$pdo = $this->desconectar();

	$pdo = $this->conectar();

	$consulta = "CREATE TABLE personas ("
		. "id INT UNSIGNED NOT NULL AUTO_INCREMENT,"
		. "nombre VARCHAR(25),"
		. "PRIMARY KEY (id));"
		. "CREATE TABLE telefonos ("
		. "id INT UNSIGNED NOT NULL AUTO_INCREMENT,"
		. "numero VARCHAR(25),"
		. "idpersona INT,"
		. "PRIMARY KEY (id),"
		. "FOREIGN KEY (idpersona) REFERENCES personas(id));";
	$pdo->query($consulta);

	$consulta = "INSERT INTO personas (nombre, apellidos) VALUES ('Pablo');"
		. "INSERT INTO personas (nombre, apellidos) VALUES ('Luis');"
		. "INSERT INTO telefonos (numero, idpersona) VALUES ('666112233', '1');"
		. "INSERT INTO telefonos (numero, idpersona) VALUES ('666998877', '2');";
	$pdo->query($consulta);

	$pdo = $this->desconectar();
    }

   

    public function idPersona() {
	$pdo = $this->conectar();  
	$id = $pdo->query("SELECT MAX(id) AS id FROM personas")->fetch(PDO::FETCH_NUM);
	$pdo = $this->desconectar();
	return $id[0] + 1;
    }

    public function idTelefono() {
	$pdo = $this->conectar();  
	$id = $pdo->query("SELECT MAX(id) AS id FROM telefonos")->fetch(PDO::FETCH_NUM);
	$pdo = $this->desconectar();
	return $id[0] + 1;
    }

      public function crearPersona($persona) {
        $pdo = $this->conectar();
	$consulta = "INSERT INTO personas (nombre) VALUES (:nombre);";
	$pdo->prepare($consulta)->execute(array(":nombre" => $persona->__GET('nombre')));
	$pdo = $this->desconectar();
          
      }

      public function crearTelefono($telefono) {
        $pdo = $this->conectar();
	$consulta = "INSERT INTO telefonos (numero, idpersona) VALUES (:numero, :idpersona);";
	$pdo->prepare($consulta)->execute(array(":numero"      => $telefono->__GET('numero'), 
						":idpersona"        => $telefono->__GET('idpersona')
						 ));
	$pdo = $this->desconectar();
      }

      public function leerPersona() {
        $personas = array();
	$pdo = $this->conectar();
	$consulta = "SELECT * FROM personas";
	$stm = $pdo->prepare($consulta);
	$stm->execute();
	foreach($stm->fetchAll(PDO::FETCH_OBJ) as $row){
	    $persona = new Persona(0, "","");
	    $persona->__SET('id', $row->id);
            $persona->__SET('nombre', $row->nombre);
                
	    $personas[] = $persona;
	}
	$pdo = $this->desconectar();
	return $personas;
      }

      public function leerTelefono() {
          
        $telefonos = array();
	$pdo = $this->conectar();
	$consulta = "SELECT * FROM telefonos";
	$stm = $pdo->prepare($consulta);
	$stm->execute();
	foreach($stm->fetchAll(PDO::FETCH_OBJ) as $row){
	    $telefono = new Telefono(0, "","","",null);
	    $persona = new Persona(0, "","");
	    
	    $persona->__SET('id', $row->idpersona);
	    
	    $telefono->__SET('id', $row->id);
	    $telefono->__SET('numero', $row->nombre);
	    $idpersona->__SET('idpersona', $row->raza);
	                   
	    $telefonos[] = $telefono;
	}
	$pdo = $this->desconectar();
	return $telefonos;
      }

  }
