<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Teléfonos</title>
	<meta charset="UTF-8">
    </head>
    <body >
	<?php
	include_once($_SERVER['DOCUMENT_ROOT'] . '/pjimenez81dwspro6/controlador/Funciones.php');
	include_once($_SERVER['DOCUMENT_ROOT'] . '/pjimenez81dwspro6/controlador/ControladorTelefono.php');
	include_once($_SERVER['DOCUMENT_ROOT'] . '/pjimenez81dwspro6/controlador/ControladorPersona.php');
	$telefono = new ControladorTelefono(); 
	$persona = new ControladorPersona();
	cabecera();
	if (Config::$modelo !== "") {
	    echo "Has escogido el modelo " . Config::$modelo . " para trabajar." ;
	}
	?>
	<h1> NUEVO TELÉFONO</h1>
        <div>
            <div>

                <form action="../controlador/ControladorGrabarTelefono.php" method="post" >
                    <table>
			<tr>
                            <th >Id.</th>
                            <td><input type="text" name="id" value="<?php $telefono->calcularIDTelefono() ?>" readonly="readonly" /></td>
                        </tr>
			<tr>
                            <th >Número</th>
                            <td><input type="text" name="numero" value="" placeholder="telefono" /></td>
                        </tr>
                        <tr>
                            <th >Id Persona</th>
                            <td><select name="idpersona">
				    <option value ="0"> Elije Propietario del Teléfono </option>
				    <?php
				    $telefono->rellenarCBPersonas();
				    ?>
                                </select></td>
                        </tr>
			<tr>
                            <td colspan="2">
                                <button type="submit">Guardar</button>
                            </td>
                        </tr>
                    </table>
                </form>

                <table>
                    <thead>
                        <tr>
			    <th>ID</th>
                            <th >Número</th>
                            <th >Id Persona</th>
                       </tr>
                    </thead>

		    
		    <?php
		    $telefono->rellenarTablaTelefono();
		    ?>
                </table>     

            </div>
        </div>
	<?php
	pie();
	inicio();
	?>
    </body>
</html>