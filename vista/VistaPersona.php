<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Gestión de Personas</title>
	<meta charset="UTF-8">
	<?php
	include_once($_SERVER['DOCUMENT_ROOT'] . '/pjimenez81dwspro6/controlador/Funciones.php');
	include_once($_SERVER['DOCUMENT_ROOT'] . '/pjimenez81dwspro6/controlador/ControladorPersona.php');
	$persona = new ControladorPersona();
	?>
    </head>
    <body >
	<?php
	cabecera();
	?>
	<h1> Nuevas Personas</h1>
        <div>
            <div>

                <form action="../controlador/ControladorGrabarPersona.php" method="post" >
                    <table >
			<tr>
                            <th >Id</th>
                            <td><input type="text" name="id" value="<?php $persona->calcularIDPersonas() ?>" readonly="readonly" s/></td>
                        </tr>
			<tr>
                            <th >Nombre</th>
                            <td><input type="text" name="nombre" value=""/></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <button type="submit">Guardar</button>
                            </td>
                        </tr>
                    </table>
                </form>

                <table>
                    <thead>
                        <tr>
			    <th>ID</th>
                            <th >Nombre</th>
                            
                        </tr>
                    </thead>

		 
		    <?php
		    $persona->rellenarTablaPersona();
		    ?>
                </table>     

            </div>
        </div>
	<?php
	pie();
	inicio();
	?>
    </body>
</html>