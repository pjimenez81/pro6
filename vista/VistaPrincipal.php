<!DOCTYPE html>
<html>
    <head>
	<title>Agenda Telefónica</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!--<style>
	    #menu {
		display: flex;
		flex-direction: row;
		flex-wrap: wrap;
		justify-content: center;
		align-items: center;
	    }
	</style>-->
    </head>
    <body>
	<?php
	    include_once($_SERVER['DOCUMENT_ROOT'] . '/pjimenez81dwspro6/controlador/Funciones.php');
	    include_once($_SERVER['DOCUMENT_ROOT'] . '/pjimenez81dwspro6/modelo/ModeloMysql.php');
	    cabecera();
	?>
	<h1> Gestionar Agenda</h1>
	    <div id="menu">
                <ul>
                    <li><a href="VistaPersona.php">Gestión de Personas</a></li>
                    <li><a href="VistaTelefono.php">Gestión de Teléfonos</a></li>
                </ul>
                
                
	    </div>
	
	<div id="documentacion">
            <h3>Documentación por tema:</h3>
            <ul>
            <li><a href="/pjimenez81dwspro6/documentacion/DWSProyectoDocumentacion.odt">Documentación en ODT</a></li>
            <li><a href="DWSProyectoT5Enunciado.pdf">Tema 5. Enunciado.</a></li>
            <li><a href="DWST6PROYECTO6.pdf">Tema 6. MYSQL.</a></li>
            
        </ul>
        	</div>
	<?php
	    pie();
	?>
    </body>
</html>
